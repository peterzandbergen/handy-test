# handy-test

Moving a tag is a simple as:

```bash
git tag -f -a prod-release
git push -f --tags
```
